#include <iostream>
#include "list.h"
using namespace std;

void main(){
	
	List a;

	for (int i = 0; i <= 6; i++) {
		a.headPush(i);
		cout << "HeadPush : " << i << endl;
	}
	
	a.headPush(6);
	cout << "HeadPush : 6 " << endl;
	a.headPush(6);
	cout << "HeadPush : 6 " << endl;

	if (a.isInList(6) == 1) {
		cout << " Found" << endl;
	}
	
	cout << "sort" << endl;

	a.Sort();
	a.Unique();

	if (a.isInList(6) == 1) {
		cout << " Found" << endl;
	}

	for (int i = 6; i > 0; i--) {
		a.headPush(i);
		cout << "HeadPush : " << i << endl;
	}

	if (a.isInList(2) == 1) {
		cout << " Found" << endl;
	}

	for (int i = 7; i < 10; i++) {
		a.tailPush(i);
		cout << "TailPush : " << i << endl;
	}

	if (a.isInList(5) == 0) {
		cout << " not Found" << endl;
	}
	else {
		cout << " Found" << endl;
	}

	cout << "Delete : 5 " << endl;

	a.deleteNode(5);

	a.Sort();

	if (a.isInList(5) == 0) {
		cout << " not Found" << endl;
	} else {
		cout << " Found" << endl;
	}

	a.Unique();
	a.Sort();


	if (a.isInList(5) == 0) {
		cout << " not Found" << endl;
	}
	else {
		cout << " Found" << endl;
	}

	system("pause");

}